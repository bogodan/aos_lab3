#include <iostream>

#include "Register.h"
#include "Processor.h"

int main() {
    std::stack<int> st;
    Processor memory;

    double number1 = 5e-3;
    double number2 = 5e-8;
    double number3 = 5e-6;
    double number4 = 5e-7;
    double number5 = 0;
    double number6 = 10e250;
    double number7 = -10e230;
//    double number5 = 0.14107050701;

    Register reg1(number1);
    Register reg2(number2);
    Register reg3(number3);
    Register reg4(number4);
    Register reg5(number5);
    Register reg6(number6);
    Register reg7(number7);


    Processor p;
    p.show_memory();
    p.push_back_to_stack(reg1);
    p.push_back_to_stack(reg2);
    p.show_stack();
    p.to_memory();
    p.show_memory();
    p.show_stack();
    p.push_back_to_stack(reg3);
    p.push_back_to_stack(reg4);
    p.show_stack();
    p.do_operation();
    p.show_stack();
    p.to_memory();
    p.push_back_to_stack(reg5);
    p.push_back_to_stack(reg6);
    p.push_back_to_stack(reg7);
    p.show_stack();
    p.show_memory();

    return 0;
}
