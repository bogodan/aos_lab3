//
// Created by bogodan on 30.11.20.
//

#ifndef AOS_LAB3_REGISTER_H
#define AOS_LAB3_REGISTER_H

#include <vector>
#include <algorithm>
#include <cmath>
#include <cassert>
#include <iostream>

class Register {
private:
    static const int characteristic;
    static const int mantissa;
    int sign;
    std::vector<int> ieee_format; // = std::vector<int>(2);
public:

    double number;
    Register(double number);
    static std::vector<int> dec_to_ieee754(double number);
    static std::vector<int> dec_to_bin(long long int dec_number, int len);
    void show_register();

};


#endif //AOS_LAB3_REGISTER_H
