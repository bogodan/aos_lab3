//
// Created by bogodan on 30.11.20.
//


#include "Register.h"

const int Register::mantissa = 5;
const int Register::characteristic = 8;

Register::Register(double number): ieee_format(std::vector<int>(128)), number(number) {
    if (number < 0){
        sign = 1;
    }
    else{
        sign = 0;
    }
    ieee_format = dec_to_ieee754(number);
}

std::vector<int> Register::dec_to_ieee754(double number) {
    std::vector<int> ieee;
    if (number < 0){
        ieee.push_back(1);
    }
    else{
        ieee.push_back(0);
    }
    if(std::abs(number) >= pow(10,pow(2,characteristic)-1 - 127) * (pow(2,mantissa)-1) ) {
        std::vector<int> charact(characteristic, 1);
        std::vector<int> mant(mantissa, 0);
        ieee.insert(ieee.end(), charact.begin(), charact.end());
        ieee.insert(ieee.end(), mant.begin(), mant.end());
        return ieee;
    }
    int exp = 0;

    while(floor(number) != number ){
        number*=10;
        exp++;
    }
//    double number_test = number;
//    while (std::to_string(number_test) == "0"){
//        exp++;
//        number_test = number_test/10;
//    }
//    assert(number_test < pow(2, mantissa)); // проверяет, можно ли записать число с такой точностью с даной битностью мантисы
    std::vector<int> mant = dec_to_bin((long long)(number), mantissa);
    std::vector<int> charact;
    if (number==0.){
        charact = dec_to_bin(0, characteristic);
    }
    else{
        charact = dec_to_bin(exp+127, characteristic);
    }
    ieee.insert(ieee.end(), charact.begin(), charact.end());
    ieee.insert(ieee.end(), mant.begin(), mant.end());

    return ieee;


}

std::vector<int> Register::dec_to_bin(long long dec_number, int len) {
    std::vector<int> res;
    if (dec_number > (long long)(pow(2, len)-1)){
        return std::vector<int>(len, 1);
    }
    if (dec_number == 0){
        return std::vector<int>(len, 0);
    }
    while (dec_number != 0){
        res.push_back(dec_number%2);
        dec_number = dec_number/2;
    }
    while(res.size() < len){
        res.push_back(0);
    }
    std::reverse(res.begin(), res.end());
    return res;
}

void Register::show_register() {
    int i = 0;
    std::cout << ieee_format[i] << " ";
    i++;
    for(; i < characteristic+1; i++){
        std::cout << ieee_format[i];
    }
    std::cout << " " << "(" << 1 << ")";
    for (; i < ieee_format.size(); i++){
        std::cout << ieee_format[i];
    }

}
