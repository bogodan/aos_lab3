//
// Created by bogodan on 30.11.20.
//

#include "Processor.h"



void Processor::push_back_to_stack(const Register& reg) {
    if (stack.size() < count_registers){
        stack.push_back(reg);
    }
}

void Processor::show_stack() {
    std::cout << "STACK:" << std::endl;
    for(auto & i : stack){
        i.show_register();
        std::cout << std::endl;
    }
    std::cout << "====================" << std::endl;
}


void Processor::to_memory() {
    if (!stack.empty() ){
        memory.push_back(stack[stack.size()-1]);
        stack.pop_back();
    }
}

void Processor::show_memory() {
    std::cout << "MEMORY IMITATION" << std::endl;
    for(auto & i : memory){
        i.show_register();
        std::cout << std::endl;
    }
    std::cout << "====================" << std::endl;
}

void Processor::operation_result() {
    while(stack.size() < 2){
        stack.push_back(Register(0));
    }
    double x = stack[stack.size()-2].number;
    double y = stack[stack.size()-1].number;
    stack.pop_back();
    exp();
    plus(2.);
    sin();
    stack.push_back(y);
    cos();
    multiply();
//    double number_res = sin(exp() + 2) * cos(y);
//    Register res(number_res);
//    return res;
}

void Processor::do_operation() {
    std::cout << "DO OPERATION WITH:" << std::endl;
    std::cout << "x = ";
    stack[stack.size()-2].show_register();
    std::cout << std::endl;
    std::cout << "y = ";
    stack[stack.size()-1].show_register();
    std::cout << std::endl;
    std::cout << "--------------------" << std::endl;
    operation_result();
    std::cout << "THE RESULT IS:" << std::endl;
    stack[stack.size()-1].show_register();
    std::cout << std::endl;
    std::cout << "--------------------" << std::endl;
//    stack.pop_back();
//    stack.pop_back();
//    stack.push_back(res);
}

void Processor::exp() {
    Register res(std::exp(stack[stack.size()-1].number));
    stack.pop_back();
    stack.push_back(res);
    std::cout << "EXP:" << std::endl;
    show_stack();
}

void Processor::plus(double number) {
    Register res(stack[stack.size()-1].number+number);
    stack.pop_back();
    stack.push_back(res);
    std::cout << "PLUS:" << number << std::endl;
    show_stack();
}

void Processor::sin() {
    Register res(std::sin(stack[stack.size()-1].number));
    stack.pop_back();
    stack.push_back(res);
    std::cout << "SIN:" << std::endl;
    show_stack();
}

void Processor::cos() {
    Register res(std::cos(stack[stack.size()-1].number));
    stack.pop_back();
    stack.push_back(res);
    std::cout << "COS:" << std::endl;
    show_stack();
}

void Processor::multiply() {
    Register res(stack[stack.size()-1].number * stack[stack.size()-2].number);
    stack.pop_back();
    stack.pop_back();
    stack.push_back(res);
    std::cout << "MULT:" << std::endl;
    show_stack();
}

//double Processor::exp(double number) {
//    return exp(stack[stack.size()-1].number);
//}
//
//double Processor::plus(double number) {
//    return stack[stack.size()-1].number+2;
//}
//
//double Processor::sin(double number) {
//    return sin(stack[stack.size()-1].number);
//}
//
//double Processor::cos(double number) {
//    return cos(stack[stack.size()-1].number);
//}
//
//double Processor::multiply(double x, double y) {
//    return stack[stack.size()-1].number*stack[stack.size()-2].number;
//}
