//
// Created by bogodan on 30.11.20.
//

#ifndef AOS_LAB3_PROCESSOR_H
#define AOS_LAB3_PROCESSOR_H

#include <stack>
#include <cmath>

#include "Register.h"

class Processor {
private:
    std::vector<Register> stack;
    std::vector<Register> memory;
    static const int count_registers = 8;
    void operation_result();

public:
    void push_back_to_stack(const Register& reg);
    void show_stack();
    void do_operation();
    void to_memory();
    void show_memory();
    void exp();
    void plus(double number);
    void sin();
    void cos();
    void multiply();

};


#endif //AOS_LAB3_PROCESSOR_H
